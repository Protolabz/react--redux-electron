import React from 'react';
import Header from '../components/header/index';

const Layout = (props) => {
    return(
        <div>
            <Header/>
            {props.children}
        </div>
    )
}

export default Layout;