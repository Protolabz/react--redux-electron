import axios from 'axios';
import { REGISTER } from "../types/userTypes";


export function isAuth(){
    
    const request = axios.get(`/api/auth`)
    .then(response => response.data)
    
    return{
        type: REGISTER,
        payload: request
    }
}