import React from 'react';
import { Switch, Route } from "react-router-dom";
import Layout from './hoc/layout';
import Home from './components/home/index';
// import SignUp from './components/signUp/signUp';
// import Login from './components/login/login';
// import Dashboard from './containers/Dashboard/dashboard';
// import ForgetPassword from './components/forgetPassword/forgetPassword';
// import ResetAccount from  './components/resetAccount/resetAccount';

const Routes = () => {
 return  ( 
     <Layout>
        <Switch>
            <Route path="/" exact component={Home}/>
            {/* <Route path="/sign-up" exact component={SignUp}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/welcome" exact component={Dashboard}/>
            <Route path="/forgot-password" exact component={ForgetPassword} />
            <Route path="/reset-account" exact component={ResetAccount} /> */}
        </Switch>
    </Layout>
    )
}

export default Routes;