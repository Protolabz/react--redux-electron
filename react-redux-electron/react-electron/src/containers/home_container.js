import React, {Component} from 'react';
import { connect } from "react-redux";
import { isAuth } from "../actions/login_actions";
import { Link } from 'react-router-dom';


class HomeContainer extends Component {
    state = { 
        email: '',
        password:'' 
    }

    componentWillMount(){
        this.props.dispatch(isAuth());
    }
    
    handleInputEmail = (event) => {
        this.setState({email: event.target.value})
    }
    handleInputPassword = (event) => {
        this.setState({password: event.target.value})
    }
    handleInputConfirmPassword = (event) => {
        this.setState({confirmPassword: event.target.value})
    }
    handleInputName = (event) => {
        this.setState({name: event.target.value})
    }
    handleInputLastname = (event) => {
        this.setState({lastname: event.target.value})
    }
    
    submitForm = (e) => {
        e.preventDefault();      
        
            // axios.post('/api/login',this.state).then(resp => {
            //     if(resp.data.isAuth === false){
            //         this.setState({error: "Wrong Email/ Password"})
            //     }else{
            //         this.setState({error: "You are Logged In"});
            //         this.props.history.push('/welcome')
            //     }

            // }).catch(console.error());
     
    }

    render() { 
        console.log(this.props);
        return ( 
            <div className="container">
                <form onSubmit={this.submitForm} className="p-3 shadow">
                    <h2 className="text-center py-2">Sign in</h2>
                    
                    <div className="form_element form-group">
                    <label>Email</label>
                        <input 
                        type="email"
                        placeholder="enter email"
                        className="form-control"
                        value = {this.state.email}
                        onChange={this.handleInputEmail}
                        />
                    </div>
                    <div className="form_element form-group">
                    <label>Password</label>
                        <input 
                        type="password"
                        placeholder="enter password"
                        className="form-control"
                        value = {this.state.password}
                        onChange={this.handleInputPassword }
                        />
                    </div>                
                    <div className="form_element form-group">
                    <button type="submit" className="btn btn-success">Submit</button>
                    </div>
                    <div className="form_element form-group">
                        <Link to="/forgot-password">
                            Forgot your password?
                        </Link>
                    </div>
                    {this.state.error ?
                    <div className="error alert alert-danger">
                        {this.state.error}
                    </div> : ''}                
                </form>
            </div> 
        );
        }
 
}
const mapStateToProps = (state) => {
    return{
        data: state.user
    }
}

export default connect(mapStateToProps)(HomeContainer);