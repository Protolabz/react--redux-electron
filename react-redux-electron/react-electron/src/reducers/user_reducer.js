import { REGISTER } from '../types/userTypes';

export default function(state={}, action){
    switch(action.type){
    case REGISTER:
    return {...state,
        user: action.payload
    }    
    default:
        return state;
    }
}