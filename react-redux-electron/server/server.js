const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const app = express();
const config = require('./config/config').get(process.env.NODE_ENV);

//Database
mongoose.Promise = global.Promise;
mongoose.connect(config.DATABASE,{ useNewUrlParser: true }).then( console.log(`Mongo Connected`)).catch(err => { throw err; });

//Models 
const {User} = require('./models/user');

//middleware 
app.use(bodyParser.json());
app.use(cookieParser());

// custom middlewares
const {auth} = require('./middlewares/auth');

app.get('/', (req,res) => {
    res.json({note: "Working"});
});


//register user 
app.post('/api/register', (req,res) => {
    const user = new User(req.body);
    user.save((err,doc) => {
        if(err) return res.json({success: false});
        res.status(200).json({
            success: true,
            user: doc
        })
    })
});

//login user
app.post('/api/login', (req,res) => {
    User.findOne({'email': req.body.email}, (err,user) => {
        if(err) return res.json({success: false});
        if(!user) return res.json({isAuth: false, message: 'Auth Failed {Email not Found}'});
        if(user){
            user.comparePassword(req.body.password,(err,isMatch)=>{
                if(err) return res.json({success: false});
                if(!isMatch) return res.json({isAuth: false, message: 'Auth Failed {Wrong Password}'});
                user.generateToken((err,user) => {
                    if(err) return res.status(400).send(err);
                    res.cookie('auth',user.token).send({
                        isAuth: true,
                        id: user._id,
                        email: user.email
                    })
                })
            }); 
          
        }      
    })
});
// logout user
app.get('/api/logout',auth,(req,res) => {
    // res.send(req.user);
    req.user.deleteToken(req.token, (err,user) => {
        if(err) return res.status(400).send(err);
        res.sendStatus(200);
    });
});

// authentication for request
app.get('/api/auth', auth, (req,res) => {
    res.send({
        isAuth: true,
        id:req.user._id,
        email:req.user.email,
        name:req.user.name,
        lastname: req.user.lastname
    })
}) 


// Application
const port = process.env.PORT || 3002;
app.listen(port, () => {
    const timing = new Date();
    console.log(`App is listing on ${port} at: ${timing}`);
})
